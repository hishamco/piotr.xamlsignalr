﻿using System;
using System.Collections.Generic;
using System.Threading;
using Microsoft.AspNet.SignalR;
using Piotr.XamlSignalR.Service.Hubs;

namespace Piotr.XamlSignalR.Service.Data
{
    internal class DataStreamThread
    {
        private const int UpdateIntervalInMilliseconds = 4000;

        private object randomLock = new object();

        private Dictionary<string, decimal> quotes = new Dictionary<string, decimal>()
                                                         {
                                                             {"EUR/USD", 1.320m},
                                                             {"GBP/USD", 1.550m},
                                                             {"AUD/USD", 1.032m},
                                                             {"PLN/USD", 0.3168m},
                                                             {"USD/JPY", 99.045m},
                                                             {"Gold", 1467.2m},
                                                             {"Silver ", 24.02m},
                                                         };

        public void Start()
        {
            Random random = new Random();

            int counter = 0;
            foreach (KeyValuePair<string, decimal> quote in quotes)
            {
                counter++;
                int id = counter;
                ThreadPool.QueueUserWorkItem(_ =>
                                                 {
                                                     var hubContext = GlobalHost.ConnectionManager.GetHubContext<QuoteHub>();

                                                     Quote q = new Quote()
                                                                   {
                                                                       Id = id,
                                                                       Name = quote.Key,
                                                                       Price = quote.Value,
                                                                       PriceChange = 0,
                                                                   };


                                                     while (true) //sic
                                                     {
                                                         double randomChange;
                                                         lock (randomLock)
                                                         {
                                                             randomChange = (random.NextDouble() - 0.42) / 100;
                                                         }
                                                         decimal change = Math.Round((decimal)randomChange * q.Price, 6);
                                                         q.Price += change;
                                                         q.PriceChange = change;
                                                         try
                                                         {
                                                             hubContext.Clients.All.updateQuote(q);
                                                         }
                                                         catch (Exception ex)
                                                         {
                                                             System.Diagnostics.Trace.TraceError("Error thrown while updating clients: {0}", ex);
                                                         }
                                                         var intervalAdj = random.Next(-700, 700);
                                                         Thread.Sleep(UpdateIntervalInMilliseconds + intervalAdj);
                                                     }
                                                 });
            }
        }
    }
}