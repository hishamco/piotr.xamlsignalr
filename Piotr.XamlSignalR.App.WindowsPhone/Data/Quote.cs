﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace Piotr.XamlSignalR.App.WindowsPhone.Data
{
    public class Quote : INotifyPropertyChanged
    {
        private string _name;
        private decimal _price;
        private decimal _priceChange;

        public int Id { get; set; }


        public string Name
        {
            get { return _name; }
            set
            {
                if (value == _name) return;
                _name = value;
                OnPropertyChanged();
            }
        }

        public decimal Price
        {
            get { return _price; }
            set
            {
                if (value == _price) return;
                _price = value;
                OnPropertyChanged();
            }
        }

        public decimal PriceChange
        {
            get { return _priceChange; }
            set
            {
                if (value == _priceChange) return;
                _priceChange = value;
                OnPropertyChanged();
                OnPropertyChanged("ChangePercentage");
            }
        }

        public double ChangePercentage
        {
            get { return (double)(_priceChange / (_price - _priceChange)) * 100; }
        }

        public event PropertyChangedEventHandler PropertyChanged;


        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
