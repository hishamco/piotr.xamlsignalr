﻿using System.Threading.Tasks;
using GalaSoft.MvvmLight.Messaging;
using Microsoft.AspNet.SignalR.Client;
using Microsoft.AspNet.SignalR.Client.Hubs;
using Piotr.XamlSignalR.App.WindowsPhone.Messaging;
using ConnectionState = Piotr.XamlSignalR.App.WindowsPhone.ViewModel.ConnectionState;

namespace Piotr.XamlSignalR.App.WindowsPhone.Data
{
    public class SignalRDataProvider : IConnectedDataProvider
    {
        private HubConnection _connection;
        private IHubProxy _hubProxy;

        private const string EndpointAddress = "http://192.168.1.18/Piotr.XamlSignalR.Service/";
        private const string StockHubName = "Quote";
        private const string QuoteUpdateName = "updateQuote";


        private readonly IMessenger _messenger;
        private readonly IDispatcher _dispatcher;

        public SignalRDataProvider(IMessenger messenger, IDispatcher dispatcher)
        {
            _messenger = messenger;
            _dispatcher = dispatcher;
            _connection = new HubConnection(EndpointAddress);
            _hubProxy = _connection.CreateHubProxy(StockHubName);
            _hubProxy.On<Quote>(QuoteUpdateName, p => _dispatcher.Dispatch(() => UpdateQuote(p)));

            _connection.StateChanged += _connection_StateChanged;
        }

        void _connection_StateChanged(StateChange stateChange)
        {
            ConnectionState oldState = ConnectionStateConverter.ToConnectionState(stateChange.OldState);
            ConnectionState newState = ConnectionStateConverter.ToConnectionState(stateChange.NewState);

            var msg = new ConnectionStateChangedMessage()
                          {
                              NewState = newState,
                              OldState = oldState,
                          };

            _dispatcher.Dispatch(() => _messenger.Send<ConnectionStateChangedMessage>(msg));
        }

        public Task StartAsync()
        {
            return _connection.Start();
        }

        private void UpdateQuote(Quote quote)
        {
            var msg = new QuoteUpdatedMessage()
                          {
                              Quote = quote
                          };
            _messenger.Send<QuoteUpdatedMessage>(msg);
        }

        public void Stop()
        {
            _connection.Stop();
        }
    }
}