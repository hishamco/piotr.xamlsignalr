﻿using Piotr.XamlSignalR.App.WindowsPhone.Resources;

namespace Piotr.XamlSignalR.App.WindowsPhone
{
    /// <summary>
    /// Provides access to string resources.
    /// </summary>
    public class LocalizedStrings
    {
        private static AppResources _localizedResources = new AppResources();

        public AppResources LocalizedResources { get { return _localizedResources; } }
    }
}